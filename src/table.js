import React from 'react';
import {
  Container,
  Table,
  Thead,
  Tbody,
  Tfoot,
  Tr,
  Th,
  Td,
  TableCaption,
  TableContainer,
  Heading,
} from '@chakra-ui/react';

function Tables() {
  return (
    <>
      <Container maxW={'7xl'} p="12">
        <TableContainer>
          <Heading textAlign="center" mb={4}>
            Table Lorem
          </Heading>
          <Table variant="striped" colorScheme="teal">
            <TableCaption>
              responsive to one column in mobile version
            </TableCaption>
            <Thead>
              <Tr>
                <Th>To convert</Th>
                <Th>into</Th>
              </Tr>
            </Thead>
            <Tbody>
              <Tr>
                <Td>Name (String)</Td>
                <Td>Title (Mr/Ms)</Td>
              </Tr>
              <Tr>
                <Td>Date of Birth (Date Picker)</Td>
                <Td>Mariage Status</Td>
              </Tr>
              <Tr>
                <Td>Place of Birth (String)</Td>
                <Td>Images (Multiple files)</Td>
              </Tr>
              <Tr>
                <Td>Address (Text)</Td>
                <Td></Td>
              </Tr>
              <Tr>
                <Td>Phone Number (Number)</Td>
                <Td></Td>
              </Tr>
              <Tr>
                <Td>Email Address (Email format)</Td>
                <Td></Td>
              </Tr>
            </Tbody>
            <Tfoot>
              <Tr>
                <Th>To convert</Th>
                <Th>into</Th>
              </Tr>
            </Tfoot>
          </Table>
        </TableContainer>
      </Container>
    </>
  );
}

export default Tables;
