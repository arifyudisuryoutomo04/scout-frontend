import React, { StrictMode } from 'react';
import * as ReactDOM from 'react-dom/client';
import App from './App';
import { ChakraProvider, theme } from '@chakra-ui/react';
import Navbar from './components/navbar';
import Footer from './components/footer';
import Hero from './components/hero';
import { store } from './redux/store';
import { Provider } from 'react-redux';

const container = document.getElementById('root');
const root = ReactDOM.createRoot(container);

root.render(
  <Provider store={store}>
    <StrictMode>
      <ChakraProvider theme={theme}>
        <Navbar />
        <Hero />
        <App />
        <Footer />
      </ChakraProvider>
    </StrictMode>
  </Provider>
);
