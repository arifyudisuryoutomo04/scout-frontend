import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Box from './Box';
import Table from './table';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Box />} />
        <Route path="/table" element={<Table />} />

        <Route path="*" element={<Box />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
