import React, { useState } from 'react';
import {
  Container,
  SimpleGrid,
  Flex,
  Button,
  useColorModeValue,
} from '@chakra-ui/react';

function randomColor() {
  return Math.floor(Math.random() * 4);
}

const colorList = ['transparent', 'red', 'green', 'blue'];

function Box(props) {
  const [colorCode, setColorCode] = useState(colorList[randomColor()]);
  const [colorCodes, setColorCodes] = useState(colorList[randomColor()]);
  const [colorCodess, setColorCodess] = useState(colorList[randomColor()]);
  return (
    <>
      <Container maxW={'7xl'} p="12">
        <SimpleGrid columns={{ base: 1, md: 3 }} spacing={10}>
          <Flex
            h="282px"
            justifyContent="center"
            alignItems="center"
            borderRadius="10px"
            bgColor={`${colorCode}`}
          >
            <Button
              {...props}
              px={8}
              bg={useColorModeValue('#151f21', 'gray.900')}
              color={'white'}
              rounded={'md'}
              _hover={{
                transform: 'translateY(-2px)',
                boxShadow: 'lg',
              }}
              onClick={() => setColorCode(colorList[randomColor()])}
            >
              Click Me
            </Button>
          </Flex>

          <Flex
            h="282px"
            justifyContent="center"
            alignItems="center"
            borderRadius="10px"
            bgColor={`${colorCodes}`}
          >
            <Button
              {...props}
              px={8}
              bg={useColorModeValue('#151f21', 'gray.900')}
              color={'white'}
              rounded={'md'}
              _hover={{
                transform: 'translateY(-2px)',
                boxShadow: 'lg',
              }}
              onClick={() => setColorCodes(colorList[randomColor()])}
            >
              Click Me
            </Button>
          </Flex>

          <Flex
            h="282px"
            justifyContent="center"
            alignItems="center"
            borderRadius="10px"
            bgColor={`${colorCodess}`}
          >
            <Button
              {...props}
              px={8}
              bg={useColorModeValue('#151f21', 'gray.900')}
              color={'white'}
              rounded={'md'}
              _hover={{
                transform: 'translateY(-2px)',
                boxShadow: 'lg',
              }}
              onClick={() => setColorCodess(colorList[randomColor()])}
            >
              Click Me
            </Button>
          </Flex>
        </SimpleGrid>
      </Container>
    </>
  );
}

export default Box;
