<div align="center">
  <a href="#">
    <img src="/public/scout.png" alt="" width="480px">
  </a>
</div>

## the application is responsive untuk mobile dan website

## Getting Started Frontend - React.js

First, install all needed dependencies:

```bash
cd frontend
```

```bash
npm install
```

Then, run the development server:

```bash
npm start
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## More

take a look at the following resources:

- [React.js](https://reactjs.org/) - about React and React Features (e.g. React Hooks)
- [Chakra UI](https://chakra-ui.com/) - Chakra UI. This project uses Chakra UI as UI Library
- [React-router-dom](https://www.npmjs.com/package/react-router-dom) - Routes
- [etc](#)
